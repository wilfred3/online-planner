from django.conf import settings
from django.urls import path
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

from online_planner import views

app_name = "online_planner"

urlpatterns = [
    path(
        "appointment_types/", views.AppointmentType.as_view(), name="appointment_types"
    ),
    path("appointments/", views.Appointment.as_view(), name="appointments"),
    path(
        "bookables/<int:appointment_type>", views.Bookable.as_view(), name="bookables"
    ),
    path("resources/", views.Resource.as_view(), name="resources"),
]

if settings.DEBUG:
    urlpatterns += [
        path(
            "swagger-ui/",
            TemplateView.as_view(
                template_name="online_planner/swagger-ui.html",
                extra_context={"schema_url": "online_planner:openapi-schema"},
            ),
            name="swagger-ui",
        ),
        path(
            "openapi",
            get_schema_view(
                title="planner API",
                version="1.0.0",
                url="/" + settings.ONLINE_PLANNER_PATH,
                # only generate the schema for the endpoints of the current app
                patterns=urlpatterns,
            ),
            name="openapi-schema",
        ),
    ]
