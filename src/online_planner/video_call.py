import datetime
import logging

import requests
from django.conf import settings
from django.utils.translation import gettext

from .settings_container import SettingsContainerBase
from .exceptions import VideoCallBackendRequestError

logger = logging.getLogger(__name__)


def get_video_call_params(request_params):
    """Return the dictionary that specifies the video call URLs.

    This function reserves a room for a video call and returns the URLs to that
    room for the host and for the guest. It uses the endpoint specified by
    Django setting VIDEO_CALL_RESERVE_ROOM_URL to retrieve them.

    """
    message = "Request to reserve room at video call backend failed"
    settings_container = SettingsContainerBase().build()

    try:
        response = requests.post(
            settings_container.get_video_api_url(), json=_build_payload(request_params)
        )
    except requests.RequestException as e:
        logger.exception(message)
        raise VideoCallBackendRequestError(
            f"{message}: POST raised {e.__module__}.{e.__class__.__name__}"
        )

    # abort if the response does not have a JSON payload: without a JSON
    # payload there are no URLs to retrieve
    try:
        response_data = response.json()
    except requests.exceptions.JSONDecodeError as e:
        log_reason = str(e)
        exception_reason = "unable to decode JSON payload of response"
        _abort_on_error(message, response.status_code, log_reason, exception_reason)

    status_dict = response_data.get("status", {})

    # abort if the JSON payload or the response status code indicate failure
    success = status_dict.get("success", False)
    if not success or response.status_code >= 400:
        # set default reason to log and raise
        log_reason = exception_reason = response.reason

        if not success:
            exception_reason = "JSON payload of response malformed or indicates failure"

        error_codes = status_dict.get("error_codes", [])
        if error_codes:
            log_reason = ", ".join(error_codes)

        _abort_on_error(message, response.status_code, log_reason, exception_reason)

    # Both the JSON payload and the response status code indicate success. This
    # means there should be an "urls" key that specifies the URLs.
    return {
        "VideoCallHostURL": response_data["urls"]["host"],
        "VideoCallGuestURL": response_data["urls"]["guest"],
    }


def _build_payload(request_params):
    """Return the payload to the endpoint that reserves a room."""
    params = request_params

    # The original request does not specify an end time, which is something
    # that will be addressed by dossier#191. For now we assume the meeting
    # should take one hour.

    time_start = datetime.time.fromisoformat(params["start_time"])

    # To compute the end time of the meeting we cannot just add a
    # datetime.timedelta of one hour to a datetime.time: Python only
    # supports adding it to a datetime.datetime.
    finish = datetime.datetime(
        2000, 1, 1, time_start.hour, time_start.minute
    ) + datetime.timedelta(hours=1)

    return {
        "date": params["date"],
        # Support for different languages will be addressed by dossier#192.
        # For now we assume the language should be Dutch.
        "language": "nl",
        "time_start": params["start_time"],
        "time_finish": finish.time().strftime("%H:%M"),
        "users": {
            # When the call to the video call backend is done, the meeting
            # isn't scheduled yet and the name of the meeting "resource" is
            # not yet known. To identify the host we hard-code a general
            # name.
            "name_host": params.get('meeting_host_name') or gettext("Employee"),
            "name_guest": params.get('meeting_guest_name') or _name_from_parameters(**params),
        },
    }


def _name_from_parameters(**params):
    if 'first_name' in params and 'last_name' in params:
        return f"{params['first_name']} {params['last_name']}"
    if 'first_name' in params:
        return params['first_name']
    if 'last_name' in params:
        return params['last_name']
    return ''


def _abort_on_error(message, status_code, log_reason, exception_reason):
    logger.error(f"{message}: {log_reason} ({status_code})")
    raise VideoCallBackendRequestError(f"{message}: {exception_reason} ({status_code})")
