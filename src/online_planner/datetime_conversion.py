"""Provide functions to convert between CET and UTC times.

Whenever this Django app uses a date & time, you can assume it's in UTC unless
explicitly stated otherwise. However, the meeting backend expects and returns
times in CET that also respect daylight saving time. This module provides the
functions to convert date & times from CET to UTC and vise-versa.

To convert a time between CET and UTC and take daylight saving time into
account is not straightforward. This module depends on third-party pytz to do
the heavy lifting. The examples in

    http://pytz.sourceforge.net/#localized-times-and-date-arithmetic.

inspired the implementation of this module.

"""
import datetime

import pytz


def as_utc_datetime(iso_date: str, iso_time: str) -> datetime.datetime:
    """Return the given CET date & time as a UTC datetime.datetime."""
    naive_date = datetime.datetime.fromisoformat(iso_date)
    naive_time = datetime.time.fromisoformat(iso_time)

    ams_tz = pytz.timezone("CET")
    ams_time = ams_tz.localize(
        datetime.datetime(
            naive_date.year,
            naive_date.month,
            naive_date.day,
            naive_time.hour,
            naive_time.minute,
        )
    )

    return ams_time.astimezone(pytz.UTC)


def as_cet_datetime(iso_date: str, iso_time: str) -> datetime.datetime:
    """Return the given UTC date & time as a CET datetime.datetime."""
    naive_date = datetime.datetime.fromisoformat(iso_date)
    naive_time = datetime.time.fromisoformat(iso_time)

    cet_time = pytz.UTC.localize(
        datetime.datetime(
            naive_date.year,
            naive_date.month,
            naive_date.day,
            naive_time.hour,
            naive_time.minute,
        )
    )

    return cet_time.astimezone(pytz.timezone("CET"))
