"""Provide a REST-like interface to plan entries in an online agenda.

onlineafspraken.nl is a service that allows you to manage an agenda, e.g. to
schedule an appointment or to delete an already scheduled one. The planner app
provides a REST interface to that service that is easier to use than the one
provided by onlineafspraken.nl. In the remainder of this document and also in
the code, the term "meeting backend" is used to denote onlineafspraken.nl.

This app provides the following REST endpoints:

planner/appointment_types
  retrieve the available appointment types

planner/resources
  retrieve the available resources

planner/bookables/<int:appointment_type>
  retrieve the time slots that are available for the given appointment type
  from today until the end of next month

planner/appointments/
  schedule an appointment that is specified by the information in the payload

If you have the server running, you can see an overview of all endpoints and
interact with them at reverse('online_planner:swagger-ui'). This functionality
comes with Django app `Django REST Framework`_ (DRF), which is used to
implement the endpoints.

The endpoints access `endpoints of the meeting backend`_ to retrieve or store
their information. For example, planner/appointment_types returns the content
that is retrieved from meeting backend endpoint getAppointmentTypes_.

Endpoint planner/bookables deserves a special mention: it is the only endpoint
that returns the information received from the meeting backend in a different
format. Where endpoint getBookableTimes_ returns a list of items that each
specify an available date and time, planner/bookables aggregates this
information per date. This makes it easier for the frontend to determine which
time slots are available for a given day.

You need an account at onlineafspraken.nl to be able to use the service. That
account comes with an API secret and an API key to "sign" each request to the
service. The planner app automatically signs the request using the secret and
key provided by Django settings ONLINE_MEETINGS_API_SECRET and
ONLINE_MEETINGS_API_KEY.

If the request to a planner app endpoint is well-formed, its response is a dict
that looks like this::

    { "status":
        { "success": <bool>,  # True if and only if the request succeeded
          "message": <str>    # specifies reason of request failure
        },
        "items": [ <dict> ]   # list of dicts, where each element specifies one
                              # type of content
    }

There are scenarios when you will get a standard HTTP error instead, for
example, you will get a 400 if the request lacks a required parameter.

Successful access to a planner endpoint depends on successful access to the
meeting backend. There are there are 2 types of errors that can occur:

- the connection cannot be establised or the request fails with a status code
  that indicates an error, or
- the request succeeds, even has a status code that indicates success, but the
  "Status/Status" field of the XML response specifies something else than
  "success".

For example, if you use an expired API key to access the meeting backend, the
request itself will succeed, but the response content will indicate that the
request could not be fullfilled. If that happens, the planner endpoint will
return a 200 HTTP status code and use the "status" dict of the response to
specify the error.

Do note that these scenarios do not cover all "unhappy paths".

Remarks:

- It's not yet possible to modify or cancel an appointment.

.. _Django REST Framework: https://www.django-rest-framework.org/
.. _endpoints onlineafspraken.nl: https://onlineafspraken.nl/nl_NL/developers/referentie
.. _getAppointmentTypes: https://onlineafspraken.nl/nl_NL/developers/referentie#getAppointmentTypes
.. _getBookableTimes: https://onlineafspraken.nl/nl_NL/developers/referentie#getBookableTimes

"""

__version__ = "1.3.3"
