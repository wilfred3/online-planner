from django.conf import settings
from django.utils.module_loading import import_string


class SettingsContainerBase:
    @staticmethod
    def build():
        try:
            class_object = import_string(settings.ONLINE_MEETINGS_SETTINGS_CONTAINER)
            return class_object()
        except AttributeError:
            return DefaultSettingsContainer()

    def get_key(self):
        raise NotImplementedError()

    def get_secret(self):
        raise NotImplementedError()

    def get_url(self):
        raise NotImplementedError()

    def get_video_api_url(self):
        raise NotImplementedError()


class DefaultSettingsContainer(SettingsContainerBase):
    def get_key(self):
        return settings.ONLINE_MEETINGS_API_KEY

    def get_secret(self):
        return settings.ONLINE_MEETINGS_API_SECRET

    def get_url(self):
        return settings.ONLINE_MEETINGS_URL

    def get_video_api_url(self):
        return settings.VIDEO_CALL_RESERVE_ROOM_URL
