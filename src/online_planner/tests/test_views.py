import unittest
import unittest.mock as mock

import pytest
import requests
import responses
import time_machine
from django.conf import settings
from django.test import Client
from django.urls import reverse

from .responses import (
    AGENDAS_CONTENT,
    APPOINTMENT_TYPES_CONTENT,
    APPOINTMENTS_PUT_CONTENT,
    BOOKABLES_CONTENT,
    CUSTOMERS_CONTENT,
)
from online_planner.settings_container import SettingsContainerBase


class AssertExpectedItems:
    def assertExpectedItems(self, expected_items, actual_items):
        """Assert that each expected item is present in the list of actual items.

        Both parameters are lists of dictionaries. This method asserts that an
        item of the expected list of dictonaries "is a subset of" an element in
        the same position in the list of actual dictionaries. "To be a subset
        of" means that each key-value pair of an expected dictionary is also
        present in the corresponding actual dictionary.

        """
        self.assertEqual(len(expected_items), len(actual_items))
        for expected, actual in zip(expected_items, actual_items):
            self.assertExpectedAttributes(expected, actual)

    def assertExpectedAttributes(self, expected_element, actual_element):
        """Assert that each key-value of the expected element is present in the actual
        element.

        This method does not check that both items contain the same key-value
        pairs. By focussing on a subset of attributes, the test should be more
        robust for changes in the actual element.

        """
        self.assertLessEqual(expected_element.items(), actual_element.items())


class TestAppointmentType(AssertExpectedItems, unittest.TestCase):
    @mock.patch("requests.get")
    def test_returns_expected_payload(self, requests_get):
        response = mock.Mock()
        response.status_code = 200
        response.text = APPOINTMENT_TYPES_CONTENT
        requests_get.return_value = response

        c = Client()
        response = c.get(reverse("appointment_types"))

        self.assertEqual(200, response.status_code)

        expected_content = {
            "status": {"success": True, "message": ""},
            "items": [
                {"Id": 1234, "Name": "name A", "Description": "description A"},
                {"Id": 5678, "Name": "name B", "Description": "description B"},
            ],
        }
        actual_content = response.json()
        self.assertEqual(expected_content["status"], actual_content["status"])
        self.assertExpectedItems(expected_content["items"], actual_content["items"])

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.get")
    def test_response_payload_indicates_failure_when_request_specifies_error(
            self, requests_get
    ):
        response = mock.Mock()
        response.status_code = 404
        response.reason = "HttpException"
        requests_get.return_value = response

        c = Client()
        response = c.get(reverse("appointment_types"))

        self.assertEqual(200, response.status_code)

        expected_content = {
            "status": {
                "success": False,
                "message": "Request to meeting backend failed: HttpException (404)",
            },
            "items": [],
        }
        actual_content = response.json()

        self.assertEqual(expected_content["status"], actual_content["status"])
        self.assertEqual(expected_content["items"], actual_content["items"])

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.get")
    def test_response_payload_indicates_failure_when_meeting_backend_cannot_be_reached(
            self, requests_get
    ):
        requests_get.side_effect = requests.exceptions.ConnectionError

        c = Client()
        response = c.get(reverse("appointment_types"))

        self.assertEqual(200, response.status_code)

        expected_content = {
            "status": {
                "success": False,
                "message": "Request to meeting backend failed: ConnectionError",
            },
            "items": [],
        }
        actual_content = response.json()

        self.assertEqual(expected_content["status"], actual_content["status"])
        self.assertEqual(expected_content["items"], actual_content["items"])


class TestBookables(AssertExpectedItems, unittest.TestCase):
    def setUp(self):
        super().setUp()
        self.settings = SettingsContainerBase().build()

    # This test also verifies that the dates & times from now until (and
    # including) 30 days later are requested from the meeting backend. To make
    # that slightly more interesting, we set "now" to 2020-02-20, which is a
    # leap year.
    @time_machine.travel("2020-02-20")
    @mock.patch("requests.get")
    def test_returns_expected_payload(self, requests_get):
        requests_get.side_effect = self.requests_get

        c = Client()
        response = c.get(reverse("bookables", args=[42]))

        items = response.json()["items"]

        self.assertEqual(
            1,
            len(items),
            "our own endpoint should return the times for 1 date",
        )

        self.assertEqual(
            "2022-04-21",
            items[0]["Date"],
            "our own endpoint should return the times for the correct date",
        )

        # verify that the times returned are converted to UTC.
        self.assertEqual(
            [
                {"StartTime": "07:00", "EndTime": "07:30"},
                {"StartTime": "07:30", "EndTime": "08:00"},
                {"StartTime": "08:00", "EndTime": "08:30"},
                {"StartTime": "08:30", "EndTime": "09:00"},
                {"StartTime": "09:00", "EndTime": "09:30"},
                {"StartTime": "09:30", "EndTime": "10:00"},
                {"StartTime": "10:00", "EndTime": "10:30"},
                {"StartTime": "10:30", "EndTime": "11:00"},
                {"StartTime": "11:00", "EndTime": "11:30"},
                {"StartTime": "11:30", "EndTime": "12:00"},
                {"StartTime": "12:00", "EndTime": "12:30"},
                {"StartTime": "12:30", "EndTime": "13:00"},
                {"StartTime": "13:00", "EndTime": "13:30"},
                {"StartTime": "13:30", "EndTime": "14:00"},
                {"StartTime": "14:00", "EndTime": "14:30"},
                {"StartTime": "14:30", "EndTime": "15:00"},
            ],
            items[0]["Times"],
        )

        expected_params = {
            "AgendaId": 51,
            "AppointmentTypeId": 42,
            "Date": "2020-02-20",
            "EndDate": "2020-03-21",
            "method": "getBookableTimes",
        }

        call_args = requests_get.call_args_list
        self.assertGreater(len(call_args), 0)

        _, kwargs = call_args[-1]  # destructure arguments of last call

        self.assertExpectedAttributes(expected_params, kwargs["params"])

    @pytest.mark.usefixtures("only_log_critical")
    def test_returns_404_without_appointment_type(self):
        c = Client()
        # We cannot user "reverse" here to test access to the bookables/
        # endpoint without appointment type. The reverse lookup itself fails
        # when the appointment type is missing.
        response = c.get("/planner/bookables/")
        self.assertEqual(404, response.status_code)
        self.assertEqual("Not Found", response.reason_phrase)

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.get")
    def test_returns_200_if_backend_cannot_be_reached(self, requests_get):
        requests_get.side_effect = requests.exceptions.ConnectionError

        c = Client()
        response = c.get(reverse("bookables", args=[42]))
        self.assertEqual(200, response.status_code)

        actual_content = response.json()

        self.assertFalse(actual_content["status"]["success"])
        self.assertEqual(
            "Request to meeting backend failed: ConnectionError",
            actual_content["status"]["message"],
        )
        self.assertEqual([], actual_content["items"])

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.get")
    def test_returns_200_if_backend_returns_an_error(self, requests_get):
        response = mock.Mock()
        response.status_code = 404
        requests_get.return_value = response

        c = Client()
        response = c.get(reverse("bookables", args=[42]))
        self.assertEqual(200, response.status_code)

        actual_content = response.json()

        self.assertFalse(actual_content["status"]["success"])
        self.assertTrue(
            actual_content["status"]["message"].startswith(
                "Request to meeting backend failed: "
            )
        )
        self.assertTrue(actual_content["status"]["message"].endswith("(404)"))
        self.assertEqual([], actual_content["items"])

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.get")
    def test_returns_200_if_backend_response_content_specifies_an_error(
            self, requests_get
    ):
        response = mock.Mock()
        response.status_code = 200
        response.text = """
<?xml version="1.0" encoding="UTF-8"?>
<Response>
  <Status>
    <APIVersion>1.0</APIVersion>
    <Date>2022-04-22 16:38:57</Date>
    <Timestamp>1650638337</Timestamp>
    <Message>API salt not provided.</Message>
    <Code>120</Code>
    <Status>failed</Status>
  </Status>
</Response>
        """.lstrip()

        requests_get.return_value = response

        c = Client()
        response = c.get(reverse("bookables", args=[42]))
        self.assertEqual(200, response.status_code)

        expected_content = {
            "status": {
                "success": False,
                "message": "Meeting backend response content specifies a failure: API salt not provided. (120)",
            },
            "items": [],
        }
        actual_content = response.json()

        self.assertEqual(expected_content["status"], actual_content["status"])
        self.assertEqual(expected_content["items"], actual_content["items"])

    @staticmethod
    def requests_get(*args, **kwargs):
        assert (
                "params" in kwargs
        ), "requests.get should have been called with keyword parameter 'params'"
        params = kwargs["params"]

        assert (
                "method" in params
        ), "keyword parameter 'params' to requests.get should specify the method"
        method = params["method"]

        response = mock.Mock()
        response.status_code = 200

        if method == "getAgendas":
            response.text = AGENDAS_CONTENT
        elif method == "getBookableTimes":
            response.text = BOOKABLES_CONTENT

        return response


class TestAppointments(AssertExpectedItems, unittest.TestCase):
    @responses.activate()
    @mock.patch("requests.get")
    def test_returns_expected_payload(self, requests_get):
        requests_get.side_effect = self.requests_get

        # configure the expected request to meeting backend method setAppointment

        expected_query_params = {
            "AgendaId": 51,
            "AppointmentTypeId": 412410,
            "CustomerId": 35366364,
            "Date": "2022-04-29",
            "Phone": "0612345678",
            # note the conversion from "14:30" (UTC) to "16:30" (CET)
            "StartTime": "16:30",
            "method": "setAppointment",
        }
        # query_params does not contain the keys needed to sign the request as
        # some of them depend on the current time stamp. Their values are
        # tested by the unit tests for function sign_params.
        responses.put(
            self.settings.get_url(),
            match=[
                responses.matchers.query_param_matcher(
                    expected_query_params, strict_match=False
                )
            ],
            status=201,
            body=APPOINTMENTS_PUT_CONTENT,
        )

        c = Client()
        response = c.post(reverse("appointments"), self.valid_payload)
        self.assertEqual(201, response.status_code)

        expected_content = {"items": [{"Id": 126328623, "Status": "2"}]}
        actual_content = response.json()
        self.assertExpectedItems(expected_content["items"], actual_content["items"])

    @property
    def valid_payload(self):
        return {
            "appointment_type": 412410,
            "date": "2022-04-29",
            "start_time": "14:30",  # UTC
            "first_name": "Hello",
            "last_name": "World",
            "phone": "0612345678",
            "email": "hello.world@example.com",
        }

    @pytest.mark.usefixtures("only_log_critical")
    def test_return_400_when_multiple_required_parameters_are_missing(self):
        c = Client()
        payload = self.valid_payload
        del payload["appointment_type"]
        del payload["date"]
        response = c.post(reverse("appointments"), payload)
        self.assertEqual(400, response.status_code)
        self.assertEqual("Bad Request", response.reason_phrase)
        self.assertEqual({"appointment_type", "date"}, set(response.json().keys()))

    @pytest.mark.usefixtures("only_log_critical")
    @mock.patch("requests.get")
    def test_return_200_if_backend_cannot_be_reached(self, requests_get):
        requests_get.side_effect = requests.exceptions.ConnectionError

        c = Client()
        response = c.post(reverse("appointments"), self.valid_payload)
        self.assertEqual(200, response.status_code)

        actual_content = response.json()

        self.assertFalse(actual_content["status"]["success"])
        self.assertEqual(
            "Request to meeting backend failed: ConnectionError",
            actual_content["status"]["message"],
        )
        self.assertEqual([], actual_content["items"])

    @staticmethod
    def requests_get(*args, **kwargs):
        assert (
                "params" in kwargs
        ), "requests.get should have been called with keyword parameter 'params'"
        params = kwargs["params"]

        assert (
                "method" in params
        ), "keyword parameter 'params' to requests.get should specify the method"
        method = params["method"]

        response = mock.Mock()
        response.status_code = 200

        if method == "getAgendas":
            response.text = AGENDAS_CONTENT
        elif method == "getCustomers":
            response.text = CUSTOMERS_CONTENT
            assert ("Email", "hello.world@example.com") in kwargs[
                "params"
            ].items(), "keyword parameter 'params' to requests.get should specify the email address"

        return response
