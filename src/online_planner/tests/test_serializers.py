import unittest

# no type hints exist for ddt
import ddt

from online_planner.serializers import AppointmentSerializer


@ddt.ddt
class TestAppointmentSerializer(unittest.TestCase):
    def test_considers_valid_payload_as_valid(self):
        serializer = AppointmentSerializer(data=self.valid_payload)
        self.assertTrue(serializer.is_valid())

    @property
    def valid_payload(self):
        return {
            "appointment_type": 412410,
            "date": "2022-05-23",
            "start_time": "10:30",
            "first_name": "Donald",
            "last_name": "Duck",
            "phone": "0612345678",
            "email": "donald.duck@example.com",
        }

    @ddt.data(
        "appointment_type",
        "date",
        "start_time",
        "first_name",
        "last_name",
        "phone",
        "email",
    )
    def test_considers_a_payload_that_lacks_a_key_to_be_invalid(self, missing_key):
        payload = self.valid_payload
        del payload[missing_key]

        serializer = AppointmentSerializer(data=payload)

        self.assertFalse(serializer.is_valid())
        self.assertEqual([missing_key], list(serializer.errors.keys()))

    @ddt.data(
        ("appointment_type", "should be a number"),
        ("date", "23-05-2022"),  # only YYYY-MM-DD is allowed
        ("start_time", "10:30.201"),  # only HH:MM is allowed
        ("email", "@example.com"),
    )
    def test_considers_a_payload_with_invalid_value_to_be_invalid(self, value):
        existing_key, new_value = value

        payload = self.valid_payload
        payload[existing_key] = new_value

        serializer = AppointmentSerializer(data=payload)
        serializer.is_valid()

        self.assertFalse(serializer.is_valid())
        self.assertEqual([existing_key], list(serializer.errors.keys()))

    def test_considers_a_payload_with_string_appointment_type_to_be_valid(self):
        payload = self.valid_payload
        # this may come as a surprise: even though the serializer specifies an
        # integer type, a string value will also pass its validation
        payload["appointment_type"] = "412410"

        serializer = AppointmentSerializer(data=payload)
        serializer.is_valid()

        self.assertTrue(serializer.is_valid())
