import os

ROOT_URLCONF = "online_planner.urls"

SECRET_KEY = "not so secret"

MIDDLEWARE = (
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
)

INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.staticfiles",
    "rest_framework",
    "online_planner",
)

# Logging config

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "style": "{",
            "format": "{levelname} {asctime} {name} {message}",
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    # By default, log records that are created in a subpackage or third-party
    # library, propagate to the root logger. Key "root" configures how that
    # logger handles the log records that reach it.
    "root": {
        # discard any record with a lower level than set here
        "level": "WARNING",
        "handlers": ["console"],
    },
    # Key "loggers" configures how to handle log records that are created in a
    # subpackage or module.
    "loggers": {
        # the next entry is mostly there as an example
        "online_planner.views": {
            # If "propagate" is False, only the "current" logger will handle
            # the log record: it will not be handled "higher up", for example
            # by the root logger.
            #
            # If "propagate" is True, the same log record is handled here
            # and "higher up".
            "propagate": False,
            # Discard any record with a lower level than set here: use a low
            # level such as "DEBUG" or "TRACE" to debug.
            "level": "WARNING",
            "handlers": ["console"],
        }
    },
}

# * Required settings for online-planner

# path to online-planner app
#
# online-planner needs this path to let the Django REST Framework generate the
# correct URLs on the Swagger page. Without it, that page assumes the endpoints
# are placed at the ROOT_URLCONF instead of ROOT_URLCONF/online-planner.

ONLINE_PLANNER_PATH = "online-planner"

# ** onlineafspraken.nl

# secrets to sign requests to onlineafspraken.nl
#
# SECURITY WARNING: keep these values used in production secret!
ONLINE_MEETINGS_API_KEY = os.getenv("ONLINE_MEETINGS_API_KEY", "<undefined>")
ONLINE_MEETINGS_API_SECRET = os.getenv("ONLINE_MEETINGS_API_SECRET", "<undefined>")

# base URL of the endpoints of onlineafspraken.nl
ONLINE_MEETINGS_URL = "https://agenda.onlineafspraken.nl/APIREST"

# ** Video room endpoint

# URL of the endpoint to allocate a room for a video call
VIDEO_CALL_RESERVE_ROOM_URL = "https://test.videocall.pleio.wonderbit.com/api/v1/room/"

# ** Django REST Framework

REST_FRAMEWORK = {
    # By default the Swagger UI shows you an endpoint with a text field for a
    # JSON payload. Here we re-order the default set of parsers so the Swagger
    # UI shows you that endpoint with a form that properly documents the
    # required parameters.
    "DEFAULT_PARSER_CLASSES": [
        "rest_framework.parsers.MultiPartParser",
        "rest_framework.parsers.JSONParser",
        "rest_framework.parsers.FormParser",
    ],
    # only allow access to authenticated users
    # "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
    # only render the request & response data as JSON, don't render the browseable API
    # "DEFAULT_RENDERER_CLASSES": ["rest_framework.renderers.JSONRenderer"],
    # limit the number of requests to the API
    # "DEFAULT_THROTTLE_CLASSES": ["rest_framework.throttling.UserRateThrottle"],
    # "DEFAULT_THROTTLE_RATES": {"user": "50/day"},
    # examples of valid dates: "2022-05-20", "2022-12-06"
    "DATE_INPUT_FORMATS": ["%Y-%m-%d"],
    "DATE_FORMAT": "%Y-%m-%d",
    # examples of valid times: "09:30", "14:00"
    "TIME_INPUT_FORMATS": ["%H:%M"],
    "TIME_FORMAT": "%H:%M",
    # The DRF DateField and DateTimeField don't follow their own documentation:
    # even with the INPUT formats above, the validation allows input dates and
    # times without a zero-padded month, day, hour or min. Keep this in mind
    # when you depend on that padding.
}
