PROJECT_ROOT := $(abspath $(dir $(firstword $(MAKEFILE_LIST))))

on-branch-change: static-analysis run-tests

static-analysis: black flake8 isort mypy

black:
	black --check	src/

flake8:
	flake8 src/

isort:
	isort --check src/

mypy:
	mypy src/

run-tests:
	pytest src/
