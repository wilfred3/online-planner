# online-planner

`online-planner` is a Django app which provides a REST-like interface to onlineafspraken.nl, a SaaS to manage an online agenda. This interface is custom-made
for the use cases of Stichting Pleio and as such easier to use, but also more limited, than
the [interface provided by onlineafspraken.nl](https://onlineafspraken.nl/developers).

In the remainder of this document and also in the code, the term "meeting backend" is used to denote onlineafspraken.nl.

## Getting started

This repository implements a Django app and that needs to be installed by a Django project to use it. However, part of the development of `online-planner`
can be done without such a project as the unit tests run without any external dependencies. To run these unit tests, create a virtualenv, install the
dependencies of `dev-requirements.txt` and execute command `pytest src/` from the root of the repo.

At Pleio, Django project [Dossier](https://gitlab.com/pleio/dossier) installs
`online-planner` and you can use that project to test the Django app during development. Its README describes how to start an instance of the `Dossier`
website and how to access the Swagger page with the endpoints of
`online-planner`.

### Django settings

This section describes the Django settings that `online-planner` uses. You can find example settings in
the [Django settings file](src/online_planner/tests/settings.py) that is used by the unit tests, section "Required settings for online-planner".

`online_planner` relies on another Django app to implement its endpoints, namely
[Django REST Framework](https://www.django-rest-framework.org/). To use it, add it to the Django `INSTALLED_APPS` and configure it using its Django setting
`REST_FRAMEWORK`.

The aforementioned example settings file shows some of the available keys of
`REST_FRAMEWORK`, such as `DATE_INPUT_FORMATS` and `TIME_INPUT_FORMATS`. Special mention should go to the following keys:

- `DEFAULT_PERMISSION_CLASSES`
- `DEFAULT_RENDERER_CLASSES`
- `DEFAULT_THROTTLE_CLASSES`
- `DEFAULT_THROTTLE_RATES`

When uncommented, these keys and their values make sure usage of the endpoints is restricted to authenticated users and throttled in case of overuse. Without
these settings, any user of the Django project will be able to access and use the endpoints.

To schedule an appointment that requires a virtual room for a video call,
`online-planner` reaches out to another another external service, whose endpoint should be specified by Django setting `VIDEO_CALL_RESERVE_ROOM_URL`.

You need an account at onlineafspraken.nl to use its service. That account comes with a "secret" and "key" that you use to sign each request to the service.
`online-planner` does that for you and offers two methods to make the key and secret known to the app.

1. By means of Django settings

| Key                           | Description                         |
|-------------------------------|-------------------------------------|
| `ONLINE_MEETINGS_API_SECRET`  | Copy from onlineafspraken.nl        |
| `ONLINE_MEETINGS_API_KEY`     | Copy from onlineafspraken.nl        |
| `ONLINE_MEETINGS_API_URL`     | https://<tenant>.onlineafspraken.nl |

2. By means of a custom client

When the key, secret and url are available at runtime you can provide a custom config client.

* Provide a setting `ONLINE_MEETINGS_CONFIG_CLIENT`, that contains `<path-to-library>.<class-name>`
* Implement a subclass of `SettingsContainerBase`, similar to the next example.

```python
from online_planner.settings_container import SettingsContainerBase


class SettingsContainer(SettingsContainerBase):
    def get_key(self):
        return ...

    def get_secret(self):
        return ...

    def get_url(self):
        return ...

    def get_video_api_url(self):
        return ...
```

As mentioned in section [Getting Started](#Getting-started), `online-planner`
needs to be installed in a Django project. It's the URLConf of that project that defines the exact URL of each endpoint. For example,

    urlpatterns = [
        # ...
        path("/online-planner/", include("online_planner.urls")),
        # ...
    ]

prefixes the path to each endpoint with `/online-planner`. The Django REST Framework needs to know that prefix to be able to generate the correct links on the
Swagger page. To do so, you let Django setting `ONLINE_PLANNER_PATH` hold that prefix and `online-planner` will pick it up and pass it to the Django REST
Framework. This means that the URLConf of the Django project can look like this:

    urlpatterns = [
        # ...
        path(f"{settings.ONLINE_PLANNER_PATH}/", include("online_planner.urls")),
        # ...
    ]

### Custom appointment fields at onlineafspraken.nl

When you schedule an appointment using `online-planner`, it stores the phone number of the customer with the appointment and, if the appointment requires a
video call, the URLs associated with that call. By default onlineafspraken.nl doesn't store this information and you have to manually define the appointment
attributes that will hold this information. Wiki
page [Add videocall URL to confirmation email](https://gitlab.com/pleio/online-planner/-/wikis/Add-videocall-URL-to-confirmation-email)
shows how to define such attributes and how to use them in emails that onlineafspraken.nl sends. Note that you will have to add the following three attributes:

- `Phone`
- `VideoCallGuestURL`
- `VideoCallHostURL`

## Development environment

The development virtualenv provides several tools to help you maintain the quality of the code:

- [black](https://black.readthedocs.io/en/stable/) to format Python code in such a way that it is [PEP8](https://www.python.org/dev/peps/pep-0008/)-compliant;
- [flake8](https:://flake8.pycqa.rog/en/latest/) to verify your code is PEP8-compliant;
- [isort](https://pycqa.github.io/isort/) to sort the order of your imports;
- [mypy](http://mypy-lang.org/) to validate any Python type hints.

flake8 might seems superfluous when you also have black. However it finds cases not (yet) covered by black and does some other thing, e.g., warn for unused
imports and variables.

The root of the repo contains a Makefile with several targets to run these tools for you. For example, `make static-analysis` runs all these tools::

    (py39-dev) ➜  make static-analysis
    black --check   src/
    All done! ✨ 🍰 ✨
    13 files would be left unchanged.
    flake8 src/
    isort --check src/
    mypy src/
    Success: no issues found in 13 source files
    (py39-dev) ➜

If you want you can activate the Git pre-push hook to run black, flake8 and isort on the files changed by the commits to be pushed. For more information about
that, have a look at [README-pre-commit.md](README-pre-commit.md).

The project uses [pytest](https://docs.pytest.org/en/stable/index.html) to develop and execute the unit tests. The Makefile contains a target to execute them::

    (py39-dev) ➜  make run-tests
    pytest src/
    ===================================================================== test session starts =====================================================================
    platform linux -- Python 3.9.9, pytest-7.1.1, pluggy-1.0.0
    django: settings: online_planner.tests.settings (from ini)
    rootdir: /home/swinkels/repos/git/pleio/online-planner, configfile: setup.cfg
    plugins: django-4.5.2, time-machine-2.6.0
    collected 34 items

    src/online_planner/tests/test_online_meetings.py ...........                                                                                            [ 32%]
    src/online_planner/tests/test_serializers.py ............                                                                                               [ 67%]
    src/online_planner/tests/test_views.py ...........                                                                                                      [100%]

    ===================================================================== 34 passed in 0.51s ======================================================================
    (py39-dev) ➜

`online-planner` makes use of GitLab CI/CD to these execute both `make` targets when you push your local commits. Here pipeline `static-analysis`
executes `make static-analysis` and pipeline `test` executes `make run-tests`.
