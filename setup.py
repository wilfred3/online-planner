import io
import pathlib
import re

from setuptools import find_packages, setup

current_dir = pathlib.Path(__file__).parent
# Versions should comply with PEP440.  For a discussion on single-sourcing
# the version across setup.py and the project code, see
# https://packaging.python.org/en/latest/single_source_version.html
version = "0.0.0"
with open(current_dir / "src" / "online_planner" / "__init__.py") as f:
    rx = re.compile('__version__ = "(.*)"')
    for line in f:
        m = rx.match(line)
        if m:
            version = m.group(1)
            break

setup(
    name="django-online-planner",
    version=version,
    license="EUPL",
    description="A high-level interface to onlineafspraken.nl",
    long_description=io.open("README.md", encoding="utf-8").read()
    if pathlib.Path("README.md").exists()
    else "",
    long_description_content_type="text/markdown",
    author="Pieter Swinkels",
    author_email="pieter@pleio.nl",
    url="https://gitlab.com/pleio/online-planner",
    package_dir={"": "src"},
    packages=find_packages(where="src", exclude=["online_planner.tests"]),
    keywords=["Django", "REST"],
    install_requires=[
        "Django>=3.2",
        "djangorestframework>=3.13",
    ],
    include_package_data=True,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Topic :: Office/Business :: Scheduling",
        "Programming Language :: Python :: 3.6",
    ],
)
